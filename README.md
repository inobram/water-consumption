# Bem vindo ao teste de habilidades InoBram.

Este teste tem como objetivo avaliar suas habilidades com versionamento, lógica de programação e a sua familiaridade com a linguagem de programação;

## Avisos antes de começar

Você deve fazer um fork desse repositório e deixar o projeto em modo publico para que possamos acompanhar seu histórico de commits.

# O Projeto

A descrição do problema está no contexto da pergunta disponibilizada em seu desafio. Sinta-se à vontade para escolher a linguagem C, Python, Java, etc.
No cabeçalho se sua implementação ou aqui mesmo nesse README, pedimos que descreva as ferramentas que devemos utilizar e os passos que devemos seguir para executar sua aplicação.


## Desafio

Seu desafio possui o cabeçalho de uma função:

```
water_consumption_reading (int last_read_time_interval, int last_measurement)
```

> **last_read_time_interval**: intervalo de tempo entre a leitura atual e anterior, valor em **min**;  

> **last_measurement**: ultimo valor lido da sonda em **ml**;

As entradas dever ser simuladas pela aplicação e as saídas devem ser o cosumo de agua em **ml** por **hora**, **dia**, **mês** e **ano**;

# Critérios de avaliação

O projeto apresentado nesse desafio será avaliado nos seguintes quesitos:

* **Estrutura do projeto** - Como o projeto está dividido; Como o código está organizado; Como o código está comentado e a documentação do projeto como um todo (80%)

* **Versionamento do projeto** - Quantidade e qualidade de commits - (20%)